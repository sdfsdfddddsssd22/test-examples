# ====================== Script example ======================
import re
import xlrd
from typing import Dict, List, Pattern
from datetime import datetime

from chalicelib._shared.aws import S3
from chalicelib._setup import sentry, get_logger
from chalicelib.utility import do_mapping, Key, Value

from FileBased.sftp_supplier import SFTPSupplier
from FileBased.kehe.kehe_contants import KeheContants
from FileBased.kehe.sftp_infra_kehe_mapper_products import ProductsReader
from FileBased.kehe.parser_810 import Parser810


logger = get_logger(__name__)


class KeheSupplier(SFTPSupplier):
    name = "INFRA KeHE"
    your_supplier_id = None
    filename_prefix = "KeHE"
    username = "kehe"
    _sqs_queue_name = "sftp-kehe"
    required_file_path_components: set = {"username", "type"}
    file_type_abbr_map = {
        "catalog": "FPRODUCT",
        "invoice": "INV",
    }

    def __init__(self):
        super().__init__()
        self.incoming_file_regex: Pattern = re.compile(
            r"""
            # Example file path: "kehe/upload/FPRODUCT_I09_16" where 09 is tier and 16 is DC
            (?P<username>[\w-]+)                              # Username consisting of word characters and hyphens
            (/\.test)?                                        # File can optionally be in the "/.test" subfolder
            /upload/                                          # File must be in upload subfolder
            (?P<type>[A-Z]+)                                  # File type (usually an all-caps abbreviation)
            _""",
            re.VERBOSE | re.IGNORECASE,
        )  # Verbose mode ignores pattern spaces and comments, search is case-insensitive

    @classmethod
    def supplier_catalog_products_to_ofbcn_catalogs(
        cls, supplier_catalog_products: List[dict]
    ) -> List[dict]:
        pass

    @classmethod
    def supplier_customer_to_ofbcn_customer(
        cls, supplier_customer: Dict[str, any]
    ) -> Dict[str, any]:
        pass

    @classmethod
    def supplier_line_item_to_po_file_row(
        cls, order: Dict[str, any], line_item: Dict[str, any]
    ) -> Dict[str, any]:
        pass

    @classmethod
    def supplier_product_to_ofbcn_product(cls, your_product: dict):
        pass

    @classmethod
    def check_confirmation_line_status(
        cls, s3_object: object, line_item: Dict[str, any]
    ) -> bool:
        pass

    @classmethod
    def preprocess_file(
        cls,
        file_type: str,
        file_path_components: dict,
        message: object,
        s3_object: object,
    ) -> bool:
        file_path_split = s3_object.key.rsplit("/", 1)
        if not len(file_path_split):
            sentry.send_error(
                msg="Couldn't find file path from S3 object",
                extras={"cls": cls, "s3_object": s3_object},
            )
        file_path = file_path_split[0]  # for example: 'kehe/.test/upload'
        filename = file_path_split[1]
        if filename[0:9] == "FPRODUCT_":
            timestamp = datetime.now().isoformat(sep=" ", timespec="seconds")
            new_file_s3_key = f"{file_path}/{file_path_split[1]} received {timestamp}"
            S3.rename_file(s3_object, new_file_s3_key)

    def parse_customers(cls, infra_kehe_customers: List[dict]):
        ofbcn_customers = []
        for infra_kehe_customer in infra_kehe_customers:
            kehe_account_id = infra_kehe_customer["Account Number"]
            infra_member = infra_kehe_customer["INFRA HQ Corporate Name"]
            infra_location = infra_kehe_customer["INFRA Location #"]
            infra_buyer_identifier_including_account = (
                f"{infra_member}-{infra_location}-{kehe_account_id}"
            )
            dc_num: int = int(float(infra_kehe_customer["DC"]))
            if dc_num not in KeheContants.known_dcs:
                raise ValueError(
                    f"Unexpected DC value: {dc_num}. Row={infra_kehe_customer}"
                )
            your_supplier_id: str = f"KEHE DC{dc_num:02d}"

            ofbcn_customer = do_mapping(
                source_dict=infra_kehe_customer,
                mapping_dest_source={
                    "your_supplier_id": Value(your_supplier_id),
                    "account_id": Key(
                        "Account Number", convert_type=int
                    ),
                    "location_name": Key("INFRA Location #"),
                    "store_number": Value(
                        infra_buyer_identifier_including_account
                    ),
                },
            )
            ofbcn_customers.append(ofbcn_customer)
        return ofbcn_customers

    def read_customers_xlsx(self, filename) -> List[dict]:
        workbook = xlrd.open_workbook(filename)

        worksheet = None
        for sheet in workbook.sheets():
            if sheet.name.find("COMPLETE INFRA") != -1:
                worksheet = sheet
                break
        rows = []  # The row where we stock the name of the column
        row_column_headers = 1
        for col in range(worksheet.ncols):
            rows.append(worksheet.cell_value(row_column_headers, col))
        # transform the workbook to a list of dictionnary
        data = []
        for row in range(1, worksheet.nrows):
            elm = {}
            for col in range(worksheet.ncols):
                cell_type = worksheet.cell_type(row, col)
                val = worksheet.cell_value(row, col)
                if cell_type == 2:  # float
                    val = str(val)
                elm[rows[col]] = val
            data.append(elm)
        return data[1:]

    def synch_customers(self, filename_customers):
        spreadsheet_data = infra_kehe_syncher.read_customers_xlsx(filename_customers)
        ofbcn_customers = infra_kehe_syncher.parse_customers(spreadsheet_data)
        self.ofbcn_supply_side_api.put_customers_batch(ofbcn_customers)

    def synch_invoices(self, filename_810):
        parser_810 = Parser810()
        invoices = parser_810.parse(filename_810)
        query_params = {"your_supplier_id": invoices[0]["your_supplier_id"]}
        # "your_supplier_id" param is required to keep backend logic for getting buyer_id
        self.ofbcn_supply_side_api.put_invoices_batch(
            query_params=query_params, invoices=invoices
        )

        if invoices:
            query_params = {"your_supplier_id": invoices[0]["your_supplier_id"]}
            # "your_supplier_id" param is required to keep backend logic for getting buyer_id
            self.ofbcn_supply_side_api.put_invoices_batch(
                query_params=query_params, invoices=invoices
            )

    def parse_products(self, table_data: List[Dict]):
        products = []
        return products

    def synch_products(self, filename):
        products_reader = ProductsReader(filename)
        ofbcn_products = products_reader.read_products()
        start = 0
        chunk_size = 1000
        num_products = len(ofbcn_products)
        while start < num_products:
            self.ofbcn_supply_side_api.put_products_batch(
                ofbcn_products[start : start + chunk_size]
            )
            start += chunk_size
            logger.info(
                f"Synched {min(start, num_products)} of {num_products} products"
            )


# ====================== Custom SQL func ======================
def get_purchases_query_supporting_warehouse(sql_helper, start, end):
    sql_helper.set_tables_string(
        "buyers, suppliers, orders, products, line_items, line_item_changes"
        " LEFT JOIN warehouses on line_item_changes.warehouse_ofbcn_id = warehouses.ofbcn_id"
    )
    sql_helper.set_projection_string(
        """
            products.name AS product_name
            , line_items.product_ofbcn_id
            , line_item_changes.quantity_desired AS quantity
            , products.packaging AS packaging
            , DATE_FORMAT(orders.order_date, '%Y-%m-%d') purchase_date
            , line_items.line_total_at_time_order_placed_adjusted AS line_total
            , suppliers.name AS supplier_name, warehouses.city AS warehouse_city
            , warehouses.latitude AS source_latitude
            , warehouses.longitude AS source_longitude
            , products.organic
            """
    )
    sql_helper.append_where(
        """
            line_items.order_guid = orders.order_guid
            AND suppliers.ofbcn_id = orders.supplier_ofbcn_id
            AND line_items.product_ofbcn_id = products.ofbcn_id
            AND line_item_changes.line_item_ofbcn_id = line_items.ofbcn_id
            AND orders.buyer_ofbcn_id=buyers.ofbcn_id
            AND orders.order_date >= '{}'
            AND orders.order_date < '{}'
            """.format(
            start, end
        )
    )

