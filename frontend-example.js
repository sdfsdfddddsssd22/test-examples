// ====================== Controller example (for Ember.js on ecma) ======================

import Ember from 'ember';
import Controller from '@ember/controller';
import utils from '../utils/utils';

export default Controller.extend({
  filters_service: Ember.inject.service('filters'),
  currentSortColumn: "order_date",
  loadData() {
    this.get('target').send("actionRefreshData");
  },
  actions: {
    onSort(sortBy) {
      if (sortBy === this.get("currentSortColumn")) {
        if (this.currentSortDirection === "default") {
          this.set("currentSortDirection", "reverse");
        }
        else {
          this.set("currentSortDirection", "default");
        }
      }
      else {
        this.set("currentSortColumn", sortBy);
        this.set("currentSortDirection", "default");
      }
      this.loadData();
    },
    datePickerClosed() {
      const datePicker = Ember.$("#dateRangePicker")[0]._flatpickr;
      const selectedDates = datePicker.selectedDates;
      if (selectedDates.length === 2) {
        const start = utils.formatDate(selectedDates[0]);
        const end = utils.formatDate(selectedDates[1]);
        const filters_service = Ember.get(this, "filters_service");
        filters_service.set("rangeStart", start);
        filters_service.set("rangeEnd", end);
        this.loadData();
      }
    }
  }
});


// ====================== Template example (for Ember.js) ======================

{{#if model.error_message}}
  {{model.error_message}}
{{else}}

  <div class="small-vertical-space"></div>

  <div style="margin-left: 20px">
    <h3>Assign catalogs</h3>
    <div class="small-vertical-space" />
    Customer: <select id="catalog-selector" class="select-buyer-for-catalog" {{action 'onCatalogChange' on='change'}}>
    {{#each model.all_purchasers key="@incatalog-selectordex" as |item|}}
      <option value='{{item}}' selected={{is-equal item selectedValue}}>
        {{#if item.is_parent}}
          {{item.name}}
        {{else if item.has_parent}}
          &nbsp; &nbsp; {{item.name}} &nbsp; &nbsp;
        {{else}}
          {{item.name}}
        {{/if}}
      </option>
    {{/each}}
  </select>
    <div class="medium-vertical-space" />
    {{#each model.catalogs as |item|}}
      <div>
        <input class="my-checkbox" type="checkbox"
               checked={{item.is_member}}
                 {{action "toggleMembership" item}} /> {{item.name}}
      </div>
    {{/each}}
  </div>
  <div class="small-vertical-space"></div>
  <div id="myGrid" style="height: 500px;" class="ag-theme-balham"></div>
  <div class="small-vertical-space"></div>
{{/if}}